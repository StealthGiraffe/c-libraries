#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#define elif else if

typedef enum Type {
    Int,
    Char,
    Float,
    Sock
} Type;

char* getTypeName(Type t) {
    char * toReturn = calloc(sizeof(char), 10);

    if(t == Int) {
        strncpy(toReturn, "Int", 4);
    }else if(t == Sock) {
        strncpy(toReturn, "Sock", 5);
    }
    return toReturn;
}

typedef struct Primitive {
    void* data;
    Type type;
} Primitive;

Primitive * newInt(int value) {
    Primitive * toReturn = calloc(sizeof());
}

int main() {

    Primitive bob;
    bob.type = Sock;
    printf("%s\n", getTypeName(bob.type));
}